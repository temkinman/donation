﻿using DonationApp.Web.Models;
using System.Net;
using System.Net.Mail;

namespace DonationApp.Web.Core
{
    public class EmailSender
    {
        public static bool SendEmail(Donater donater)
        {
            MailAddress from = new MailAddress("temkinman@mail.ru", "Donate's organisation", System.Text.Encoding.UTF8);
            MailAddress to = new MailAddress(donater.Email);
            
            using MailMessage message = new MailMessage(from, to)
            {
                Subject = "Платеж успешно завершен",
                Body = $"<p>Большое спасибо вам {donater.Name} за пожертвование.<br/> Ваша сумма {donater.Amount}$ дойдет до адресата.</p>",
                IsBodyHtml = true,
                BodyEncoding = System.Text.Encoding.UTF8,
                SubjectEncoding = System.Text.Encoding.UTF8
            };

            using SmtpClient client = new SmtpClient("smtp.mail.ru", 587)
            {
                Credentials = new NetworkCredential("temkinman@mail.ru", "1hLHLLiZi4tEYUwvWrHb"),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false
            };

            try
            {
                client.Send(message);
            }
            catch (System.Exception)
            {

               return false;
            }
            
            return true;
        }
    }
}
