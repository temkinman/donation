﻿using DonationApp.Web.Models;
using System.Collections.Generic;
using System.Linq;

namespace DonationApp.Web.Core
{
    public class Repository
    {
        private static readonly List<Donater> _donaters = new();

        public static List<Donater> Donaters => _donaters;
        public static void AddDonater(Donater donater) => _donaters.Add(donater);

        public static decimal AllSumOfDotanions() => _donaters.Sum(d => d.Amount);
    }
}
