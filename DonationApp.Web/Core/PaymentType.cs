﻿using System.ComponentModel.DataAnnotations;

namespace DonationApp.Web.Core
{
    public enum PaymentType
    {
        [Display(Name = "Банковская карта")]
        Card,

        [Display(Name = "Электронные деньги")]
        WebMoney,

        [Display(Name = "Криптовалюта")]
        CryptoMoney
    }
}
