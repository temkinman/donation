using DonationApp.Web.Core;
using DonationApp.Web.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DonationApp.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateInitialData();
            CreateHostBuilder(args).Build().Run();
        }

        public static void CreateInitialData()
        {
            List<Donater> donaters = new List<Donater>()
            {
                new Donater()
                {
                    Name = "�����",
                    Email = "igor@mail.ru",
                    Payment = PaymentType.Card,
                    Amount = 25,
                    Comment = "��� �������� ����� ������",
                    Date = new DateTime(2022, 04, 01)
                },

                new Donater()
                {
                    Name = "����",
                    Email = "dima@mail.ru",
                    Payment = PaymentType.WebMoney,
                    Amount = 94,
                    Comment = "��� �������� ��������",
                    Date = new DateTime(2022, 04, 10)
                },

                new Donater()
                {
                    Name = "�����",
                    Email = "pavel@mail.ru",
                    Payment = PaymentType.CryptoMoney,
                    Amount = 360,
                    Comment = "��� ��������� ����� ��������",
                    Date = new DateTime(2022, 04, 13)
                },
            };
            Repository.Donaters.AddRange(donaters);
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
