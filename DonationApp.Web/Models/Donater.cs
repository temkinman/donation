﻿using DonationApp.Web.Core;
using System;
using System.ComponentModel.DataAnnotations;

namespace DonationApp.Web.Models
{
    public class Donater
    {
        //public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Укажите имя")]
        [StringLength(30, ErrorMessage = "Имя должно не меньше 2 и не более 30 символов.", MinimumLength = 2)]
        [Display(Name="Имя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Укажите электронную почту")]
        [RegularExpression("^([\\w\\.\\-]+)@([\\w\\-]+)((\\.(\\w){2,3})+)$", ErrorMessage = "Не корректный е-мэйл")]
        [Display(Name = "Е-мэйл")]
        public string Email { get; set; }

        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

        [Required(ErrorMessage = "Укажите вид платежа")]
        [Display(Name="Вид платежа")]
        public PaymentType Payment { get; set; }

        [Required(ErrorMessage = "Укажите объем платежа")]
        [Display(Name = "Количество")]
        [RegularExpression("^\\d*\\.?\\d*$", ErrorMessage = "Введите число")]
        public decimal Amount { get; set; } = 0;

        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name ="Дата платежа")]
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
