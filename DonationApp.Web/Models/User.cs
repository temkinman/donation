﻿using System.ComponentModel.DataAnnotations;

namespace DonationApp.Web.Models
{
    public class User
    {
        [Display(Name = "Логин")]
        [Required(ErrorMessage = "Заполните поле логин")]
        public string Login { get; set; }

        [Display(Name = "Пароль")]
        [Required(ErrorMessage = "Заполните поле пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
