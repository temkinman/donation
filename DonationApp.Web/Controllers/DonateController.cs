﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using DonationApp.Web.Models;
using DonationApp.Web.Core;

namespace DonationApp.Web.Controllers
{
    public class DonateController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create([FromForm] Donater donater)
        {
            if(donater == null)
            {
                return View();
            }

            Repository.AddDonater(donater);

            if(EmailSender.SendEmail(donater))
            {
                return View("CompletePayment", donater);
            }

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
