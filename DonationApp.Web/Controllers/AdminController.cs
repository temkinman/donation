﻿using DonationApp.Web.Core;
using DonationApp.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace DonationApp.Web.Controllers
{
    public class AdminController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult LoginUser([FromForm] User user)
        {
            if(ModelState.IsValid)
            {
                if(user.Login.Trim().ToLower() == "admin" && user.Password == "admin")
                {
                    return View("Statistic", Repository.Donaters);
                }
                else
                {
                    return View("AccessDenied");
                }
            }
            return View(nameof(Index));
        }
    }
}
